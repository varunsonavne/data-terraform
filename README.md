######## Data Fetch Value for Custom tag given ###########

data "aws_subnets" "selected" {
  filter {
    name   = "tag:Name"
    values = ["aws-private-subnet"] # insert values here
  }
}

######## Data Fetch Value for Inbuild/prebuild tage given #########
data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Tier = "Private"
  }
}
