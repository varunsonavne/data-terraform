terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source    = "./vpc"
}

module "sg" {
  source = "./sg"
  depends_on = [
    module.vpc
  ]
}

module "rds" {
  source = "./rds"
  depends_on = [
    module.sg
  ]
  
}
