
data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["new"]
  }
}


resource "aws_security_group" "default" {
  name   = var.sg_name
  vpc_id = data.aws_vpc.selected.id

  dynamic "ingress" {
    for_each = var.ports
    iterator = port
    content {
      description = "TLS from VPC"
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = var.ingress_cidr_blocks
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidr_blocks
  }

  tags = {
    Name = "${var.env}"
  }
}